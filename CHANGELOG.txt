User Edit Notification Module
=============================

version 1.3 - Mon August 10 05:08:18 EST 2009
===========
- Updated to work with Drupal 6.

version 1.2 - Thu May 8 03:51:02 EST 2008
===========
- Added ability to configure destination address for notification emails. It falls back on site_mail or sendmail_from.
- Added help hook.
- Added access control hook.


version 1.1 - Thu Dec 20 21:32:41 EST 2007
===========

- Email notifications now include profile information and user email.

version 1.0 - Wed Dec 12 22:44:45 EST 2007
===========

Initial checkin. Incomplete.
